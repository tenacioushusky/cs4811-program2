# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        #print ("Succussor:\n" + str(successorGameState))
        #print ("Postion: " + str(newPos))
        #print ("Food:\n" + str(newFood))
        #print ("Ghosty boi: " + str(newGhostStates))
        #print ("Scared bois: " + str(newScaredTimes))

        # List of available food
        new_food_list = newFood.asList()
        #print new_food_list

        shortest_food = 99999999
        for food in new_food_list:
          food_manhat = util.manhattanDistance(newPos, food)
          if shortest_food > food_manhat:
            shortest_food = food_manhat
        
        ghost_positions = successorGameState.getGhostPositions()
        #print ghost_positions
        closest_ghost_distance = 99999999
        scared_ghost = False
        zip_ghosts = zip(ghost_positions, newScaredTimes)
        for ghost, time in zip_ghosts:
          ghost_distance = util.manhattanDistance(newPos, ghost)
          if(closest_ghost_distance > ghost_distance and time == 0):
            closest_ghost_distance = ghost_distance
            scared_ghost = False
          elif(closest_ghost_distance > ghost_distance and time >= 1):
            closest_ghost_distance = ghost_distance
            scared_ghost = True

        if closest_ghost_distance == 0:
          closest_ghost_distance += 1
        
        if not scared_ghost and closest_ghost_distance > 2:
          return successorGameState.getScore() + (10/shortest_food)
        if not scared_ghost and closest_ghost_distance == 1:
          return successorGameState.getScore() - 10*(closest_ghost_distance)
        if scared_ghost:
          return successorGameState.getScore() + (1/closest_ghost_distance)

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game

          gameState.isWin():
            Returns whether or not the game state is a winning state

          gameState.isLose():
            Returns whether or not the game state is a losing state
        """

        pacman_agent = 0  # pacman will always be 0
        ghost_agents = range(1, gameState.getNumAgents())  # 1 - many ghost agents
        reasonably_abnormally_large_variable = 999999999

        # Have to make functions so I can call them from within

        # Gets a list of actions that aren't stop
        def listOfActions(state, agent_num):
            action_list = list()
            legal_actions = state.getLegalActions(agent_num)
            for action in legal_actions:
                if action != Directions.STOP:
                    action_list.append(action)
            return action_list

        def termNode(state, depth):
            # Need to check if the current node to be expanded is a Win State, lose state, or no more depth
            return state.isWin() or state.isLose() or depth < 0

        def maxNode(state, depth):
            if termNode(state, depth):
                return self.evaluationFunction(state)

            # Set v to negative infinity
            v = -reasonably_abnormally_large_variable

            # For each action in legal actions that isn't stop.
            for action in listOfActions(state, pacman_agent):
                # Get the successors of PacMan
                succ = state.generateSuccessor(pacman_agent, action)
                # Take the max of all the mins
                v = max(v, minNode(succ, depth - 1))

            return v

        def minNode(state, depth):
            if termNode(state, depth):
                return self.evaluationFunction(state)

            # Set v to infinity
            v = reasonably_abnormally_large_variable

            # For every ghost
            for ghost in ghost_agents:
                for action in state.getLegalActions(ghost):  # Look at the actions ghost can make
                    #  Get successors for that ghost
                    succ = state.generateSuccessor(ghost, action)
                    # Pick the min of the Max's below you
                    v = min(v, maxNode(succ, depth - 1))

            return v

        #  Run the important parts from maxNode first
        #  Using lambda expressions to get a list of legal actions to make from the current state.
        actions = listOfActions(gameState, pacman_agent)
        results = [(action, minNode(gameState.generateSuccessor(pacman_agent, action),
                                    self.depth)) for action in actions]
        action, value = max(results, key=lambda t: t[1])  # Find the max in all of results based on the index at 1
        return action



class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

